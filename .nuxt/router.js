import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _b1ceff4e = () => interopDefault(import('../pages/dashbord.vue' /* webpackChunkName: "pages/dashbord" */))
const _1b1663fc = () => interopDefault(import('../pages/inspire.vue' /* webpackChunkName: "pages/inspire" */))
const _5d88e1de = () => interopDefault(import('../pages/login.vue' /* webpackChunkName: "pages/login" */))
const _603ee42b = () => interopDefault(import('../pages/register.vue' /* webpackChunkName: "pages/register" */))
const _0793ebfa = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/dashbord",
    component: _b1ceff4e,
    name: "dashbord"
  }, {
    path: "/inspire",
    component: _1b1663fc,
    name: "inspire"
  }, {
    path: "/login",
    component: _5d88e1de,
    name: "login"
  }, {
    path: "/register",
    component: _603ee42b,
    name: "register"
  }, {
    path: "/",
    component: _0793ebfa,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
