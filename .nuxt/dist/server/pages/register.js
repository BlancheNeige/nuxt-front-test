exports.ids = [8];
exports.modules = {

/***/ 59:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vuetify-loader/lib/loader.js??ref--4!./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--7!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/register.vue?vue&type=template&id=78cd9eb0
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c('div', {
    staticClass: "relative flex flex-col justify-center min-h-screen overflow-hidden"
  }, [_vm._ssrNode("<div class=\"w-full p-6 m-auto bg-white rounded shadow-lg lg:max-w-md\"><h1 class=\"text-3xl font-semibold text-center text-purple-700\">\n      Sign Up\n    </h1> <form class=\"mt-6\"><div><label for=\"name\" class=\"block text-sm text-gray-800\">name</label> <input name=\"name\" type=\"text\"" + _vm._ssrAttr("value", _vm.form.name) + " class=\"block w-full px-4 py-2 mt-2 text-purple-700 bg-white border rounded-md focus:border-purple-400 focus:ring-purple-300 focus:outline-none focus:ring focus:ring-opacity-40\"></div> <div><label for=\"email\" class=\"block text-sm text-gray-800\">Email</label> <input name=\"email\" type=\"email\"" + _vm._ssrAttr("value", _vm.form.email) + " class=\"block w-full px-4 py-2 mt-2 text-purple-700 bg-white border rounded-md focus:border-purple-400 focus:ring-purple-300 focus:outline-none focus:ring focus:ring-opacity-40\"></div> <div class=\"mt-4\"><div><label for=\"password\" class=\"block text-sm text-gray-800\">Password</label> <input name=\"password\" type=\"password\"" + _vm._ssrAttr("value", _vm.form.password) + " class=\"block w-full px-4 py-2 mt-2 text-purple-700 bg-white border rounded-md focus:border-purple-400 focus:ring-purple-300 focus:outline-none focus:ring focus:ring-opacity-40\"></div> <div><label for=\"password\" class=\"block text-sm text-gray-800\">Password Confirmation</label> <input name=\"password_confirmation\" type=\"password\"" + _vm._ssrAttr("value", _vm.form.password_confirmation) + " class=\"block w-full px-4 py-2 mt-2 text-purple-700 bg-white border rounded-md focus:border-purple-400 focus:ring-purple-300 focus:outline-none focus:ring focus:ring-opacity-40\"></div> <div class=\"mt-6\"><button type=\"submit\" class=\"w-full px-4 py-2 tracking-wide text-white transition-colors duration-200 transform bg-purple-700 rounded-md hover:bg-purple-600 focus:outline-none focus:bg-purple-600\">\n            \n            Submit\n          </button></div></div></form></div>")]);
};
var staticRenderFns = [];

// CONCATENATED MODULE: ./pages/register.vue?vue&type=template&id=78cd9eb0

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/register.vue?vue&type=script&lang=js
/* harmony default export */ var registervue_type_script_lang_js = ({
  auth: "guest",
  data() {
    return {
      form: {
        name: null,
        email: null,
        password: null,
        password_confirmation: null
      },
      errors: []
    };
  },
  mounted() {
    this.$axios.$get("/sanctum/csrf-cookie");
  },
  methods: {
    register() {
      try {
        this.$axios.post("/register", this.form).then(res => {
          this.$auth.loginWith("laravelSanctum", {
            data: this.form
          });
          this.$router.push({
            path: "/"
          });
        });
      } catch (err) {
        console.log(err);
      }
    }
  }
});
// CONCATENATED MODULE: ./pages/register.vue?vue&type=script&lang=js
 /* harmony default export */ var pages_registervue_type_script_lang_js = (registervue_type_script_lang_js); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./pages/register.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pages_registervue_type_script_lang_js,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "4de48a64"
  
)

/* harmony default export */ var register = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=register.js.map